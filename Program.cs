﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using amelie.DAL;
using amelie.Decoding;
using amelie.IdentityDocument;
using amelie.Model;
using amelie.Schedular;
using amelie.Utils;
using ErtmsSolutions.JruDecodeApi.Request;
using ErtmsSolutions.JruDecodeApi.Result;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ServerCodecNT.Controllers;


namespace ServerCodecNT
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /* Connection and init MongoDB */
            /*and Connect BLOB are done in Startup*/

            if (args != null && args.Length > 0 && args[0].Equals("dev"))
            {
                test();
                return;
            }

            createFolders();
            Console.WriteLine("JRUDecoderLibs:{0}", GeneralOptions.JruDecoderFolder);

            CreateWebHostBuilder(args).Build().Run();
        }



        public static void test()
        {
            //JobRequest jobRequest = Runner.buildRequestTest("JRU_000403_20190515_1018.tru");
            //Logger log = new Logger(Path.Combine(GeneralOptions.DataOutputFolder,"test.log"));
            //JobResult jobResult = Runner.execute(jobRequest, log);
            //Runner.displayResultLog(jobRequest, jobResult, log);
            var builder = new ConfigurationBuilder()
                       //    .SetBasePath(env.ContentRootPath)
                           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                           .AddJsonFile("secretsdir/secrets.json", optional: true, reloadOnChange: true)
                           .AddEnvironmentVariables();

            IConfiguration Configuration = builder.Build();
            DAMongoDB.openConnectionSNCB(Configuration);
            DAMongoDB.initCollections();
            DABloB.openConnection(Configuration);
            if (!DAMongoDB.existsReferenEngineCollection())
            {
                Console.WriteLine("Create Reference Engine collection ");
                ImportReferenceEngine.run();
            }
            if (!DAMongoDB.existsIdentityDocumentCollection())
            {
                Console.WriteLine("Create Identity Document collection ");
                Creator.run();
            }
        }
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        private static void createFolders()
        {
            if (!Directory.Exists(GeneralOptions.DataOutputFolder))
            {
                Directory.CreateDirectory(GeneralOptions.DataOutputFolder);
                Console.WriteLine("Creating folder {0},", GeneralOptions.DataOutputFolder);
            }
            if (!Directory.Exists(GeneralOptions.DataInputFolder))
            {
                Directory.CreateDirectory(GeneralOptions.DataInputFolder);
                Console.WriteLine("Creating folder {0},", GeneralOptions.DataInputFolder);
            }
            if (!Directory.Exists(GeneralOptions.LogFolder))
            {
                Directory.CreateDirectory(GeneralOptions.LogFolder);
                Console.WriteLine("Creating folder {0},", GeneralOptions.LogFolder);
            }
            Console.WriteLine("Folders created done");

        }
    }
}
