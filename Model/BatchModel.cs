﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace amelie.Model
{
    public class BatchItem
    {
        public int position { get; set; }
        public string fileName { get; set; }
        public string status { get; set; }
        public int errorNumber { get; set; }
    }

    public class BatchJob
    {
        [BsonId]
        public ObjectId id { get; set; }
        public string uid { get; set; }
        public DateTime startDate { get; set; }
        public DateTime plannedStartDate { get; set; }
        public DateTime endDate { get; set; }

        public string status { get; set; }
        public int numberFilestoTreat { get; set; }
        public int numberFilesinError { get; set; }

        public int numberFilesTreated { get; set; }

        public BatchItem[] batchItems { get; set; }
        public string loglink { get; set; }
        public string filter { get; set; }
    }
 }
