﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace amelie.Model
{
    public class DecodingConfig
    {
        public DateTime valid_from { get; set; }
        public string decodeparemeter { get; set; }
        public string pid { get; set; }
    }

    public class ReferenceEngine
    {
        [BsonId]
        public ObjectId id { get; set; }
        public string HV { get; set; }
        public string NID_ENGINE { get; set; }
        public string ENGINE_TYPE { get; set; }

        public string JRU_TYPE { get; set; }

        public DecodingConfig[] decodingconfig { get; set; }

    }
}
