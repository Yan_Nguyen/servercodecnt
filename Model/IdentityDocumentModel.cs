﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace amelie.Model
{
    public class DecodingProcess
    {
        public DateTime processingDate { get; set; }
        public amelie.IdentityDocument.DecodingStatusEnum decodingStatus { get; set; }
        /*$Y pk int? this is FaillureMessageNumber, the name should be changed*/
        public int errorMessage { get; set; }
        public int numberMessage { get; set; }
        public DateTime startDateExclude { get; set; }
        public DateTime endDateExclude { get; set; }
    }
    public class IdentityDocument
    {
        [BsonId]
        public ObjectId id { get; set; }

        [BsonElement("fileDate")]
        public DateTime fileDate { get; set; }

        [BsonElement("uploadDate")]
        public DateTime uploadDate { get; set; }

        [BsonElement("fileName")]
        public string fileName { get; set; }
        [BsonElement("link")]
        public string link { get; set; }
        [BsonElement("localpath")]
        public string localpath { get; set; }
        [BsonElement("vehiculeType")]
        public string vehiculeType { get; set; }
        [BsonElement("vehiculeNumber")]
        public string vehiculeNumber { get; set; }
        [BsonElement("nidEngine")]
        public string nidEngine { get; set; }

        [BsonElement("nidEngineJRU")]
        public string nidEngineJRU { get; set; }

        [BsonElement("priority")]
        public int priority { get; set; }
        [BsonElement("decodingProcess")]
        public DecodingProcess decodingProcess { get; set; }

        [BsonElement("decoderParameter")]
        public string decoderParameter { get; set; }

        [BsonElement("dateTimeFirstMessage")]
        public DateTime dateTimeFirstMessage { get; set; }

        [BsonElement("dateTimeLastMessage")]
        public DateTime dateTimeLastMessage { get; set; }
        /*$Y why iddoc needs to have the log link?*/
        [BsonElement("loglink")]
        public string loglink { get; set; }
    }

}
