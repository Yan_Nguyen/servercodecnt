﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using amelie.DAL;
using amelie.Decoding;
using amelie.IdentityDocument;
using amelie.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ServerCodecNT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DecoderController : ControllerBase
    {
        public static Dictionary<string, CancellationTokenSource> tasks = new Dictionary<string, CancellationTokenSource>();
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("JobStart")]
        public ActionResult<string> handleJobStartRequestID([FromQuery(Name = "JobId")] string jobID)
        {
            if (jobID == null || jobID == "")
                return "Can not start Job! jobID is not valid";
            CancellationTokenSource ts = new CancellationTokenSource();
            CancellationToken tk = ts.Token;
            Console.WriteLine("Start JobID :{0}", jobID);
            tasks[jobID] = ts;
            BatchJob batchJob = DAMongoDB.findBatchByID(jobID);
            Task.Run(() => BatchExecutor.executeBatch(batchJob, tk), tk);
            return "Start Job ID:" + jobID;
        }

        [HttpGet("JobRestart")]
        public ActionResult<string> handleJobRestart([FromQuery(Name = "JobId")] string jobID)
        {
            if (jobID == null || jobID == "")
                return "Can not restart Job! jobID is not valid";
            CancellationTokenSource ts = new CancellationTokenSource();
            CancellationToken tk = ts.Token;
            Console.WriteLine("Restart JobID :{0}", jobID);
            BatchExecutor.refreshBatch(jobID);
            tasks[jobID] = ts;
            BatchJob batchJob = DAMongoDB.findBatchByID(jobID);
            Task.Run(() => BatchExecutor.executeBatch(batchJob, tk), tk);
            return "Restart Job ID:" + jobID;
        }

        /* End an interrupted batch, reset all To be started files to not being taken by a batch */
        [HttpGet("JobEnd")]
        public ActionResult<string> handleJobEnd([FromQuery(Name = "JobId")] string jobID)
        {
            if (jobID == null || jobID == "")
                return "Can not end Job! jobID is not valid";
            Console.WriteLine("Start JobEnd interrupted batch ID :{0}", jobID);
            BatchExecutor.endInterruptedBatch(jobID);
            Console.WriteLine("End JobEnd interrupted batch ID :{0}", jobID);
            return "End Job ID:" + jobID;
        }

        [HttpGet("JobCreate")]
        public ActionResult<string> handleJobStartRequestFilter([FromQuery(Name = "JobFilter")] string jobFilter)
        {
            if (jobFilter == null || jobFilter == "")
                return "Can not create Job! Filter is not valid";
            CancellationTokenSource ts = new CancellationTokenSource();
            CancellationToken tk = ts.Token;
            Console.WriteLine("Creating job with filter:{0}", jobFilter);
            BatchJob batchJob = BatchExecutor.insertBatchJob(jobFilter);
            tasks[batchJob.uid] = ts;
            Console.WriteLine("Create JobID :{0}", batchJob.uid);
            Task.Run(() => BatchExecutor.executeBatch(batchJob, tk), tk);
            return "Create Job with filter:" + jobFilter;
        }
        /*https://localhost:5001/api/decoder/CreateBatchNight?Date=2020-02-21 20:00*/
        [HttpGet("CreateBatchNight")]
        public ActionResult<string> handleBatchCreate([FromQuery(Name = "Date")] string start)
        {

            DateTime plannedStartDate = DateTime.Parse(start);
            //int status = (int) DecodingStatusEnum.OnGoing;
            //string filter = "{\"decodingProcess.decodingStatus\":" + status + "}";
            Console.WriteLine("Creating batch for date {0}", start);
            //BatchJob batchJob = BatchExecutor.insertBatchScheduledCustom(filter, plannedStartDate);
            //CancellationTokenSource ts = new CancellationTokenSource();
            //CancellationToken tk = ts.Token;
            BatchJob batchJob = BatchExecutor.insertBatchScheduled(plannedStartDate);
            //tasks[batchJob.uid] = ts;
            return "Batch Night created";
        }

        [HttpGet("CreateCollections")]
        public ActionResult<string> createCollections()
        {
            var builder = new ConfigurationBuilder()
                           //    .SetBasePath(env.ContentRootPath)
                           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                           .AddJsonFile("secretsdir/secrets.json", optional: true, reloadOnChange: true)
                           .AddEnvironmentVariables();

            IConfiguration Configuration = builder.Build();
            DAMongoDB.openConnectionSNCB(Configuration);
            DAMongoDB.initCollections();
            DABloB.openConnection(Configuration);
            if (!DAMongoDB.existsReferenEngineCollection())
            {
                Console.WriteLine("Create Reference Engine collection ");
                ImportReferenceEngine.run();
            }
            if (!DAMongoDB.existsIdentityDocumentCollection())
            {
                Console.WriteLine("Create Identity Document collection ");
                Creator.run();
            }
            Console.WriteLine("Create Collections done");
            return "Create Collections done";
        }

        [HttpGet("JobStop")]
        public ActionResult<string> handleJobStop([FromQuery(Name = "JobId")] string jobID)
        {
            if (jobID == null || jobID == "")
                return "Can not stop Job! jobID is not valid";
            Console.WriteLine("Stoping JobID :{0}", jobID);
            Console.WriteLine("Tasks:");
            foreach (KeyValuePair<string, CancellationTokenSource> kvp in tasks)
            {
                //textBox3.Text += ("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
            }
            if (tasks.ContainsKey(jobID))
            {
                Console.WriteLine("Sending Cancellation request");
                DAMongoDB.setBatchToStop(jobID);
                tasks.GetValueOrDefault(jobID).Cancel();
                tasks.Remove(jobID);
                /*$Y the batch status will be updated by batch executor*/
                return "Stopped Job ID:" + jobID;
            }
            else
            {
                Console.WriteLine("Can not find running job " + jobID);
                return "Can not find running Job ID:" + jobID;
            }
            
        }

        [HttpGet("JobLog")]
        public ContentResult handleJobLogRequest([FromQuery(Name = "JobId")] string jobID)
        {
            if (jobID == null || jobID == "")
            {
                Console.WriteLine("Can not find job to log");
                return base.Content("<div>Can not log Job! jobID is not valid</div>", "text/html");
            }
            string res = "<!DOCTYPE>" +
                            "<html>" +
                            "  <head>" +
                            "    <title>Log batch " + jobID + "</title>" +
                            "  </head>" +
                            "  <body>";
            string log = "";
            using (StreamReader sr = System.IO.File.OpenText(Path.Combine(GeneralOptions.LogFolder, jobID + ".log")))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    log = log + "<br>" + s;
                }
            }
            res = res + log + "</ body > </html>";
            return base.Content(res, "text/html");
        }

        [HttpGet("JruLog")]
        public ContentResult handleJruLogRequest([FromQuery(Name = "JruId")] string fileName)
        {
            if (fileName == null || fileName == "")
            {
                Console.WriteLine("Can not find file to log");
                return base.Content("Can not log file! fileName is not valid", "text/html");
            }
            Console.WriteLine("JruLog resquest for JruId :{0}", fileName);
            BatchJob batchJob = DAMongoDB.findBatchByFileName(fileName);
            if (batchJob == null)
            {
                Console.WriteLine("JruLog resquest: can not find batchJob for JruId :{0}", fileName);
                return base.Content("JruLog resquest: can not find batchJob for JruId :" + fileName, "text/html");
            }
            string log = "";
            Boolean startadd = false;
            using (StreamReader sr = System.IO.File.OpenText(Path.Combine(GeneralOptions.LogFolder, batchJob.loglink)))
            {
                var toche = "*********************  Starting decoding file : " + fileName + " ***********************";
                var tocheend = "*********************  End decoding file : " + fileName + " ***********************";
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {

                    if (toche.Equals(s))
                        startadd = true;
                    if (tocheend.Equals(s))
                        startadd = false;

                    if (startadd)
                        log = log + "<br>" + s;
                }
            }
            return base.Content(log, "text/html");
        }
        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
