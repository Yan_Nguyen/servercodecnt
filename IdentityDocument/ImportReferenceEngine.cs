﻿using amelie.DAL;
using amelie.Decoding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace amelie.IdentityDocument
{
    class ImportReferenceEngine
    {
//        private const string DATA_PATH = @"C:\ERTMS\work\SNCB\amelie\data\engineType.csv";
        private static string DATA_PATH = GeneralOptions.ReferenceEngineCSV;
        public static void run ()
        {
            string[] lines = File.ReadAllLines(DATA_PATH);
            foreach (string line in lines)
            {
                var id = Guid.NewGuid().ToString();
                string[] cols = line.Split(";");
                DAMongoDB.insertOneReferenceEngine(cols[0], cols[1], cols[2], cols[3], new DateTime(2016, 1, 1), cols[7], id);

            }
        }
    }
}
