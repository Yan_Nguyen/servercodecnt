﻿using System;
using System.Collections.Generic;
using System.Text;

namespace amelie.IdentityDocument
{
    public enum DecodingStatusEnum
    {
        TobeStarted,
        OnGoing,
        Successful,
        Failed,
        Double,
        Forced, // Immedate Execution
        TakenByARunningBatch
    }
}
