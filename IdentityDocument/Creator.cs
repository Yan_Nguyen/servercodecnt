﻿using amelie.DAL;
using amelie.Decoding;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
//using Excel = Microsoft.Office.Interop.Excel;

namespace amelie.IdentityDocument
{
    /* This class will import to MongoDB the identity documents info */
    public class Creator
    {
        private const string relativeInputFolder = @"SNCB/sprint1b/04108";
        private const string rootInputFolder = @"C:/data/jrudecoder/input";
        private const string engineTypeFilePath = @"C:/ERTMS/work/SNCB/amelie/data";


        private static void parseInputFolder()
        {
            string path = Path.Combine(rootInputFolder, relativeInputFolder);
            DirectoryInfo d = new DirectoryInfo(path);
            FileInfo[] files = d.GetFiles();
            foreach (FileInfo file in files)
            {
                string fileName = file.Name;
                string link = file.FullName;
                Console.WriteLine("FileName:{0}", fileName);
            }
        }

        private static int? ConvertStringToInt(string intString)
        {
            return (Int32.TryParse(intString, out int i) ? i : (int?)null);
        }
        public static void run()
        {
            List<string> allBlobFiles = DABloB.getAllFileNames();
            string uri = DABloB.getAbsoluteUri();
            foreach (var file in allBlobFiles)
            {
                Console.WriteLine("IdDoc Creator: Processing file:{0}", file);
                var idDoc = DAMongoDB.getIdDocByFileName(file);
                if (idDoc == null)
                {
                    Console.WriteLine("Creating identity document for file:{0}", file);
                    /* Parsing file name from BLOB*/
                    string[] content = file.Split("_");
                    string HV_Number = ConvertStringToInt(content[1]).ToString();
                    string stringDate = content[2] + " 12:00:00";
                    DateTime date = DateTime.ParseExact(stringDate, "yyyyMMdd HH:mm:ss", null);
                    Console.WriteLine(date.ToString());
                    string link = uri + "/" + file;
                    string localpath = Path.Combine(GeneralOptions.inputData, file);

                    /*$Y There should only be one referecen engine, otherwise must be a problem*/
                    var referenceEngines = DAMongoDB.getReferenceEngineByHV(HV_Number);

                    if (referenceEngines == null)
                    {
                        Console.WriteLine("Problem while creating identity document for file {0}, reference engine number not found {1}", file, HV_Number);
                        continue;
                    }
                    Console.WriteLine("ENGINE TYPE:{0}", referenceEngines.ENGINE_TYPE);
                    Console.WriteLine("HV:{0}", referenceEngines.HV);
                    DAMongoDB.insertOneIdDoc(file, 
                                            link, 
                                            referenceEngines.ENGINE_TYPE, 
                                            referenceEngines.HV, 
                                            DecodingStatusEnum.TobeStarted, 
                                            "", 
                                            referenceEngines.NID_ENGINE, 
                                            date, 
                                            DateTime.Now, 
                                            localpath);

                }
                else
                {
                    Console.WriteLine("Already existed");
                }
            }
        }
    }
}
