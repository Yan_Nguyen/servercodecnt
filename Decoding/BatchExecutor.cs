﻿using amelie.DAL;
using amelie.Model;
using amelie.Schedular;
using amelie.Utils;
using ErtmsSolutions.JruDecodeApi.Request;
using ErtmsSolutions.JruDecodeApi.Result;
using MongoDB.Bson;
using ServerCodecNT.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static amelie.DAL.DAMongoDB;

namespace amelie.Decoding
{
    class BatchExecutor
    {
        private static Logger log;
        private static  int nbFileTreated = 0;
        private static int nbFileFailed = 0;
        private const string TO_BE_STARTED = "To be started";
        private static BatchJob createNewBatchJob(string filterString)
        {
            string uid = Guid.NewGuid().ToString();
            return createBatchJobWithExistingUID(filterString, uid);
        }

        private static BatchJob createBatchScheduled(string filterString, DateTime psd)
        {
            BatchJob batchJob = createNewBatchJob(filterString);
            batchJob.plannedStartDate = psd;
            return batchJob;
        }

        private static BatchJob createBatchJobWithExistingUID(string filterString, string uid)
        {
            List<BatchItem> items = createBatchItemsByFilter(filterString);
            BatchJob batchJob = new BatchJob()
            {
                uid = uid,
                batchItems = items.ToArray(),
                numberFilestoTreat = items.Count,
                loglink = uid + ".log",
                filter = filterString
            };

            return batchJob;
        }

        private static List<BatchItem> createBatchItemsByFilter (string f)
        {
            var results = DAMongoDB.findIdDocByFilter(f);
            List<BatchItem> batchItems = new List<BatchItem>();
            int i = 0;
            foreach (var r in results)
            {
                BatchItem batchItem = new BatchItem
                {
                    position = i,
                    fileName = r.fileName,
                    status = TO_BE_STARTED
                };
                batchItems.Add(batchItem);
                i++;
            }
            return batchItems;
        }

        public static BatchJob insertBatchJob (string filter)
        {
            BatchJob batchJob = createNewBatchJob(filter);
            if (batchJob == null)
                Console.WriteLine("Error while creating batch with filter:{0}", filter);
            else
                DAMongoDB.insertBatchJob(batchJob);
            return batchJob;
        }

        public static BatchJob insertBatchScheduledCustom(string filter, DateTime psd)
        {
            BatchJob batchJob = createBatchScheduled(filter, psd);
            if (batchJob == null)
                Console.WriteLine("Error while creating batch night with filter:{0}", filter);
            else
                DAMongoDB.insertBatchJob(batchJob);
            return batchJob;
        }

        public static void endInterruptedBatch(string uid)
        {
            BatchJob batchJob = DAMongoDB.findBatchByID(uid);
            if (batchJob.status != DAMongoDB.INTERRUPTED)
            {
                Console.WriteLine("Can not end Batch:{0}. It is not interrupted", uid);
                return;
            }
            BatchItem[] batchItems = batchJob.batchItems;
            foreach(BatchItem batchItem in batchItems)
            {
                if (batchItem.status == TO_BE_STARTED)
                {
                    DAMongoDB.updateIdDocStatusToBeStarted(batchItem.fileName);

                }
            }
            DAMongoDB.setBatchToReleased(uid);
        }

        public static BatchJob insertBatchScheduled(DateTime psd)
        {
            int status = (int)IdentityDocument.DecodingStatusEnum.TobeStarted;//has to be chaned to tobestarted after test
            string filter = "{\"decodingProcess.decodingStatus\":" + status + "}";
            return insertBatchScheduledCustom(filter, psd);
        }
        public static BatchJob refreshBatch(string uid)
        {
            BatchJob batchJob = DAMongoDB.findBatchByID(uid);
            if (batchJob == null)
            {
                Console.WriteLine("Batch id={0} not found", uid);
                return null;
            }
            Console.WriteLine("Refreshing Found Batch uid={0}", uid);
            
            BatchJob newBatchJob = createBatchJobWithExistingUID(batchJob.filter, uid);
            DAMongoDB.refreshExistingBatchJob(newBatchJob);
            return newBatchJob;

        }

        public static void setLogFile (string file)
        {
            log = new Logger(file);
        }

        private static void interruptBatch(BatchJob batchJob)
        {
            Console.WriteLine("Stop Batch uid:{0}", batchJob.uid);
            log.addLog("Stop requested");
            DAMongoDB.setBatchToInterrupted(batchJob);
            log.addLog("\nEnd Date :" + DateTime.Now);
            log.addLog("Number of files treated : " + nbFileTreated);
            log.addLog("Number of files Failed : " + nbFileFailed);

            log.addLog("*********************  End of Batch : " + batchJob.uid + " ***********************");
            Console.WriteLine("Batch " + batchJob.uid + " interrupted.");
        }
        
        private static bool isDone (BatchItem batchItem)
        {
            Console.WriteLine("File {0} has status {1}", batchItem.fileName, batchItem.status);
            /*$Y This should be enum not string*/
            return (batchItem.status == "Success");
        }

        public static Boolean isFileToDecodeInIdDoc(string fileName)
        {
            var file = DAMongoDB.getIdDocByFileName(fileName);
            if (file == null)
            {
                Console.WriteLine("File {0} not found in Identity Documents Table", fileName);
                return false;
            }
            return true;
        }
        private static void decodeAFile (BatchItem batchItem, string uid, string logLink)
        {
            string fileName = batchItem.fileName;
            log.addLog("\n*********************  Starting decoding file : {0} ***********************", fileName);
            if (isDone(batchItem))
            {
                log.addLog("File {0} already done", fileName);
                return;
            }
            Console.WriteLine("Processing : {0}", fileName);

            /* pull the file from the Blob */
            Task t = DABloB.downloadBlobAsync(fileName, Path.Combine(GeneralOptions.DataInputFolder, fileName));
            t.Wait();
            Console.WriteLine("Path:{0}", Path.Combine(GeneralOptions.DataInputFolder, fileName));
            log.addLog("Download file {0} from Blob done.", fileName);
            Console.WriteLine("Download file {0} from Blob done.", fileName);
            resetDatesAndNumberMessages(batchItem);
            var idDoc = DAMongoDB.getIdDocByFileName(fileName);
            if (idDoc == null)
            {
                Console.WriteLine("File {0} not found in Identity Documents Table", fileName);
                log.addLog("Decoding file not found in Identity Documents Table:{0}", fileName);
                DAMongoDB.setBatchItemToFailed(uid, batchItem);
            }
            else
            {
                DAMongoDB.updateIdDocStatusOnGoing(fileName, logLink);
                DAMongoDB.setBatchItemToOnGoing(uid, batchItem);
                var decodingParam = DAMongoDB.findDecodingParameterByDate(idDoc.vehiculeNumber, idDoc.fileDate);
                log.addLog("Found JRU Decoder parameter :{0}", decodingParam);
                DAMongoDB.updateIdDocDecodingParam(fileName, decodingParam);
                log.addLog("Launch decoding\n");
                Console.WriteLine("Launch Decoding");
                JobResult jobResult = runDecoder(decodingParam, Path.Combine(GeneralOptions.DataInputFolder, fileName), log);
                log.addLog("Decoding done \n");
                Console.WriteLine("Decoding done");
                //log.addLog(jobResult.Log);
                updateAfterDecoding(uid, batchItem, jobResult);

            }
        }

        private static void updateAfterDecoding(string uid, BatchItem batchItem, JobResult jobResult)
        { 
            if (jobResult == null)
            {
                updateWhenDecodingFailed(uid, batchItem);
            }
            else
            {
                if (jobResult.Success)
                {
                    updateWhenDecodingSuccess(jobResult, uid, batchItem);
                }
                else
                {
                    updateWhenDecodingFailed(uid, batchItem);
                }
            }

            //               displayResultLog(jobRequest, jobResult);

            Runner.cleanUp(jobResult);
            log.addLog("*********************  End decoding file : " + batchItem.fileName + " ***********************");
            nbFileTreated++;
            DAMongoDB.incrementNumberFileTreated(uid);
        }

        private static void updateWhenDecodingFailed (string uid, BatchItem batchItem)
        {
            Console.WriteLine("Error after decoding file");
            log.addLog("Decoding of file {0} Failed", batchItem.fileName);
            DAMongoDB.setBatchItemToFailed(uid, batchItem);
            DAMongoDB.updateIdDocStatusFailed(batchItem.fileName);
            nbFileFailed++;
        }

        public static void updateWhenDecodingSuccess(JobResult jobResult, string uid, BatchItem batchItem)
        {
            if (jobResult is JobResultJruDecode result)
            {
                if (result.Layer1Result.Success)
                {
                    Console.WriteLine(result.Layer2Result.SuccessMessagesCounter + "  " + result.Layer2Result.FailureMessagesCounter + "   " + result.Layer2Result.SuccessMessagesCounter);
                    int successMessages = result.Layer2Result.SuccessMessagesCounter;
                    if (successMessages <= 0)
                    {
                        updateWhenDecodingFailed(uid, batchItem);
                        return;
                    }
                    DAMongoDB.updateIdDocNumbersMessages(batchItem.fileName, successMessages, result.Layer2Result.FailureMessagesCounter);
                    string fileNameNoExtension = batchItem.fileName.Split(".")[0];
                    /*$Y these info must come from decoder */
                    string nid = DAMongoDB.getNIDEngineFromDecodedMessage(fileNameNoExtension);
                    DatesMessages dates = DAMongoDB.GetFirstLastDateMessage(fileNameNoExtension);
                    DAMongoDB.updateIdDocStatusSuccessful(batchItem.fileName, nid, dates.firstDate, dates.lastDate);
                    if (result.Layer2Result.Success)
                    {
                        Console.WriteLine("Level 2 succesfull");
                        log.addLog("Decoding of file {0} Successfull", batchItem.fileName);
                        DAMongoDB.setBatchItemToSuccessfull(uid, batchItem);
                    }
                    else
                    {
                        updateWhenDecodingFailed(uid, batchItem);
                    }

                }
                else
                {
                    updateWhenDecodingFailed(uid, batchItem);
                }
                    
            }
        }
        private static JobResult runDecoder (string decodingParam, string filePath, Logger log)
        {
            JobRequest jobRequest = Runner.buildRequest(nbFileTreated.ToString(), decodingParam, filePath);
            JobResult jobResult = Runner.execute(jobRequest, log);
            Runner.displayResultLog(jobRequest, jobResult, log);
            return jobResult;
        }

        private static void initVariables()
        {
            nbFileFailed = 0;
            nbFileTreated = 0;
        }
        public static void executeBatch(BatchJob batchJob, CancellationToken token)
        {
            if (batchJob == null)
            {
                Console.WriteLine("BatchJob ID {0} not found ");
                return;
            }
            initVariables();
            BatchItem[] batchItems = batchJob.batchItems;
            setLogFile(Path.Combine(GeneralOptions.LogFolder, batchJob.loglink));
            log.addLog("\n************************************************************************************************");
            log.addLog("***********************Starting batch " + batchJob.uid + "***********************");
            log.addLog("************************************************************************************************");
            log.addLog("Number of files to be treated : " + batchItems.Length);
            log.addLog("Files to be treated:");
            foreach (BatchItem d in batchItems)
            {
                DAMongoDB.updateIdDocStatusToTakenByABatch(d.fileName);
                log.addLog(d.fileName);
            }
            log.addLog("Start Time : " + DateTime.Now.ToString());
            DAMongoDB.setBatchToStart(batchJob);

            foreach (BatchItem batchItem in batchItems)
            {
                if (token.IsCancellationRequested)
                {
                    interruptBatch(batchJob);
                    token.ThrowIfCancellationRequested();
                }
                decodeAFile(batchItem, batchJob.uid, batchJob.loglink);

            }
            DAMongoDB.setBatchToDone(batchJob);
            log.addLog("\nEnd Date :" + DateTime.Now);
            log.addLog("Number of files treated : " + nbFileTreated);
            log.addLog("Number of files Failed : " + nbFileFailed);
            log.addLog("*********************  End of Batch : " + batchJob.uid + " ***********************");
            Console.WriteLine("Batch " + batchJob.uid + " done.");
        }

        private static void resetDatesAndNumberMessages(BatchItem batchItem)
        {
            DAMongoDB.updateIdDocNumbersMessages(batchItem.fileName, 0, 0);
            DAMongoDB.updateIdDocDatesMessages(batchItem.fileName, null, DateTime.MinValue, DateTime.MinValue);
        }

        public static void ScheduleBatch(int hour, int min, int interval)
        {
            MyScheduler.IntervalInMinutes(hour, min, interval, () =>
            {
                Console.WriteLine("Looking for the next Batch Scheduled");
                BatchJob batchScheduled = DAMongoDB.getLastBatchScheduled();
                if (batchScheduled == null)
                {
                    Console.WriteLine("No Batch scheduled has been found. PlannedStartDate has not set on any of batchs");
                    return;
                }
                if (batchScheduled.plannedStartDate <= DateTime.Now && batchScheduled.status == null)
                {
                    Console.WriteLine("Found Scheduled Batch to run:{0} at {1}", batchScheduled.uid, batchScheduled.plannedStartDate);

                    DateTime nextPlannedStartDate = batchScheduled.plannedStartDate.AddDays(1);
                    Console.WriteLine("NextPlannedStartDate:" + nextPlannedStartDate);
                    BatchJob nextSchedule = insertBatchScheduled(nextPlannedStartDate);

                    CancellationTokenSource tokenSource = new CancellationTokenSource();
                    CancellationToken token = tokenSource.Token;
                    batchScheduled = refreshBatch(batchScheduled.uid);
                    if (DecoderController.tasks.ContainsKey(batchScheduled.uid))
                        DecoderController.tasks.Remove(batchScheduled.uid);

                    DecoderController.tasks.Add(batchScheduled.uid, tokenSource);



                    Task.Run(() => BatchExecutor.executeBatch(batchScheduled, token), token);
                }
                else
                {
                    Console.WriteLine("Nothing to do");
                }
            });
        }
    }
}
