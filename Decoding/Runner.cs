﻿using amelie.Utils;
using ErtmsSolutions.JruDecodeApi.Launcher;
using ErtmsSolutions.JruDecodeApi.Request;
using ErtmsSolutions.JruDecodeApi.Result;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace amelie.Decoding
{
    class Runner
    {
        private const int TimeOut = GeneralOptions.DecoderTimeOut; //minutes


        /// <summary>
        /// When the JruDecoderLauncher objects does something it send debugging messages
        /// that appear here.
        /// </summary>
        /// <param name="job_id"></param>
        /// <param name="text"></param>
        private static void log_from_launcher(string job_id, string text)
        {
            Console.WriteLine("[{0}] {1}", job_id, text);
        }

        public static JobRequest buildRequestTest(string fileName)
        {
            JobRequestJruDecode jobRequest = new JobRequestJruDecode("01", "Logiplus_TRU_NG_V3_1");

            /* We will create files in here.
             * A true aplication could (or should) create a temporary folder for each job and
             * ensure the contents are deleted after they have been processed. */
            string output_folder = GeneralOptions.DataOutputFolder;

            /* We must provide a *fully qualified* file name for all the files. */
            jobRequest.InputFile = Path.Combine(GeneralOptions.DataInputFolder, fileName);


            jobRequest.OutputFile = Path.Combine(output_folder,fileName);


            jobRequest.NotesFile = Path.Combine(output_folder, fileName + ".notes");

            jobRequest.ExportFormat = "MongoDB";

            jobRequest.VerboseLayer1 = false;

            jobRequest.VerboseLayer2 = false;

            return jobRequest;
        }

        public static JobRequest buildRequest(string jobID, string jruFormat, string filePath)
        {
            JobRequestJruDecode jobRequest = new JobRequestJruDecode(jobID, jruFormat);


            string output_folder = GeneralOptions.DataOutputFolder;

            /* We must provide a *fully qualified* file name for all the files. */
            jobRequest.InputFile = filePath;

            jobRequest.OutputFile = Path.Combine(output_folder, Path.GetFileName(jobRequest.InputFile));

            jobRequest.NotesFile = Path.Combine(output_folder, Path.GetFileName(jobRequest.InputFile) + ".notes");

            jobRequest.ExportFormat = "MongoDB";

            jobRequest.VerboseLayer1 = false;

            jobRequest.VerboseLayer2 = false;

            return jobRequest;
        }

        public static JobResult execute(JobRequest a_job_request, Logger log)
        {
            /// We use the API object that launches the decoder:
            JruDecoderLauncher jru_decoder_launcher = new JruDecoderLauncher();

            jru_decoder_launcher.DumpStdOutToConsole = true;

            jru_decoder_launcher.DumpStdErrToConsole = true;

            jru_decoder_launcher.TimeOut = TimeOut * 60.0; // Seconds

            // Set the 'log' output function and remove it after execution.
            jru_decoder_launcher.OnLog += log.writeLog;

            // Each application should know *where* is the JruFileDecoder.exe
            // This is the actual execution.
            
            jru_decoder_launcher.Execute(a_job_request, GeneralOptions.JruDecoderFolder);

            // Set the 'log' output function and remove it after execution.
            jru_decoder_launcher.OnLog -= log.writeLog;


            JobResult job_result = null;
            if (jru_decoder_launcher.ExitCode == 0)
            {
                job_result = jru_decoder_launcher.Result;
            }
            else
            {
                // We could not launch the tool or the
                // tool was not happy with its command line parameters.
                Console.WriteLine("JOB {0} has failed with exit_code = {1}", a_job_request.ID, jru_decoder_launcher.ExitCode);
            }
            return job_result;
        }

        public static void displayResultLog(JobRequest a_job_request, JobResult a_job_result, Logger log)
        {
            if (a_job_result == null) return;

            if (a_job_result.Success)
            {
                Console.WriteLine("JOB {0} success !", a_job_request.ID);
                log.addLog("JOB {0} success !", a_job_request.ID);
                // We can have a look at the messages:
                foreach (JobResultMessage msg in a_job_result.Messages)
                {
                    Console.WriteLine("JOB {0} Message : {1,5} #{2,4} {3,32}", a_job_request.ID, msg.Status, msg.Identifier, msg.Text);
                    log.addLog("JOB {0} Message : {1,5} #{2,4} {3,32}", a_job_request.ID, msg.Status, msg.Identifier, msg.Text);
                }

                // We can do something with the decoded file:
                //Console.WriteLine("JOB {0} Decoded data is in here: {1}", a_job_request.ID, a_job_request.OutputFile);
                //log.addLog("JOB {0} Decoded data is in here: {1}", a_job_request.ID, a_job_request.OutputFile);
                //        Console.WriteLine("JOB {0} Total Messages: {1,10} Successes: {2,6} Faillures: {3,6}", a_job_request.ID, a_job_result.);
                if (a_job_result is JobResultJruDecode result)
                {
                    Console.WriteLine("JOB {0} Total Messages: {1,10} Successes: {2,6} Faillures: {3,6}", a_job_request.ID, result.Layer2Result.TotalMessages, result.Layer2Result.SuccessMessagesCounter, result.Layer2Result.FailureMessagesCounter);
                    log.addLog("JOB {0} Total Messages: {1,10} Successes: {2,6} Faillures: {3,6}", a_job_request.ID, result.Layer2Result.TotalMessages, result.Layer2Result.SuccessMessagesCounter, result.Layer2Result.FailureMessagesCounter);
                }
            
                    

            }
            else
            {
                // The tool run succesfully but he declared some errors: Undecoded messages ?
                Console.WriteLine("JOB {0} fails with some errors", a_job_request.ID);
                log.addLog("JOB {0} fails with some errors", a_job_request.ID);
                foreach (JobResultMessage msg in a_job_result.Messages)
                {
                    Console.WriteLine("JOB {0} MSG {1}", a_job_request.ID, msg.ToString());
                    log.addLog("JOB {0} MSG {1}", a_job_request.ID, msg.ToString());
                }
            }
        }

        public static void cleanUp(JobResult jobResult)
        {
            if (jobResult != null)
            {
                /* A true application should, like any well-bahaved application,
                 * be careful with IO operations and encapsulate each deletion
                 * in a wise try/catch... */
                foreach (string file_name in jobResult.FilesCreated)
                {
                    Console.WriteLine("Cleaning FileName: {0}", file_name);
                    File.Delete(file_name);
                }
                /* Please note and understand the Reverse()... */
                foreach (string folder_name in jobResult.FoldersCreated.Reverse<string>())
                {
                    Console.WriteLine("Cleaning FolderName: {0}", folder_name);
                    Directory.Delete(folder_name);
                }
            }
        }
    }
}
