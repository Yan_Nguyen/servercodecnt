﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace amelie.Decoding
{
    class GeneralOptions
    {

        private static string RootPath = Environment.CurrentDirectory;
        /* Place stores generated files */
        //        public const string DataOutputFolder = @"c:\data\jrudecoder\temp";

        public static string DataOutputFolder = Path.Combine(RootPath, "temp");
        public static string inputData = "inputData";

//        public static string DataInputFolder = @"C:\data\Jrudecoder\input\SNCB";
        public static string DataInputFolder = Path.Combine(RootPath, inputData);


        private const string DecoderFolderRelativePath = "decoderLibs";

        public static string JruDecoderFolder = Path.Combine(RootPath, DecoderFolderRelativePath);

        //        public const string LogFolder = @"C:\data\Jrudecoder\log\";
        public static string LogFolder = Path.Combine(RootPath, "log");

        public static string ReferenceEngineCSV = Path.Combine(RootPath, @"data/engineType.csv");

        public const string ExportFormat = "MongoDB";

        public const int DecoderTimeOut = 30;
        public bool VerboseLayer1 = false;


        public bool VerboseLayer2 = true;
    }
}
