﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using amelie.DAL;
using amelie.Decoding;
using amelie.IdentityDocument;
using amelie.Schedular;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ServerCodecNT
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            var builder = new ConfigurationBuilder()
                           .SetBasePath(env.ContentRootPath)
                           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                           .AddJsonFile("secretsdir/secrets.json", optional: true, reloadOnChange: true)
                           .AddEnvironmentVariables();

            Configuration = builder.Build();
            DAMongoDB.openConnectionSNCB(Configuration);
            DAMongoDB.initCollections();
            DABloB.openConnection(Configuration);

            int hour, min, interval, hourIDTable, minIDTable, intervalIDTable;
            try
            {
                hour = int.Parse(Configuration["hour"]);
                min = int.Parse(Configuration["min"]);
                interval = int.Parse(Configuration["interval"]);
                hourIDTable = int.Parse(Configuration["hourIDTable"]);
                minIDTable = int.Parse(Configuration["minIDTable"]);
                intervalIDTable = int.Parse(Configuration["intervalIDTable"]);
            }
            catch (Exception)
            {

                hour = 23;
                min = 59;
                interval = 10;// every 10 minutes
                hourIDTable = 9;
                minIDTable = 00;
                intervalIDTable = 6;// every 6 hours
            }

            MyScheduler.IntervalInHours(hourIDTable, minIDTable, intervalIDTable, () =>
            {
                Console.WriteLine("We update the Identity Document List");
                Creator.run();
            });
            BatchExecutor.ScheduleBatch(hour, min, interval);
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
