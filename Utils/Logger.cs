﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace amelie.Utils
{
    public class Logger
    {
        private string filePath;
        public void addLog(string a_format, params object[] parameters)
        {
            if (filePath == "")
                return;
            string final = String.Format(a_format, parameters);
            writeLog("", final);

        }

        public void writeLog(string id, string final)
        {
            using (var fileStream = new FileStream(filePath, FileMode.Append))
            {
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    streamWriter.WriteLine(final);
                    streamWriter.Close();
                }
                fileStream.Close();

            }
        }

        public Logger (string filePath)
        {
            this.filePath = filePath;
        }
    }
}
