﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace amelie.DAL
{
    public class DABloB
    {
        private static BlobContainerClient containerClient;
        public static void openConnection(IConfiguration config)
        {
            string connectionString = config["connectionstringBlob"];

            // Create a BlobServiceClient object which will be used to create a container client
            BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);


            string containerName = config["containerBlobName"];

            // Create the container and return a container client object
            containerClient =  blobServiceClient.GetBlobContainerClient(containerName);
            
        }


        public static List<string> getAllFileNames()
        {
            List<string> res = new List<string>();
            
            foreach (BlobItem blobItem in containerClient.GetBlobs())
            {
//                Console.WriteLine("\t" + blobItem.Name);
                res.Add(blobItem.Name);
            }
            return res;
        }
        
        public static string getAbsoluteUri ()
        {
            return containerClient.Uri.AbsoluteUri;
        }
        public static async Task downloadBlobAsync(string fileName, string localPath)
        {

            try
            {
                //          Console.WriteLine("\nDownloading blob to\n\t{0}\n", localPath);

                BlobClient blobClient = containerClient.GetBlobClient(fileName);
                // Download the blob's contents and save it to a file
                BlobDownloadInfo download = await blobClient.DownloadAsync();

                Console.WriteLine("localPath:{0}", localPath);
                FileStream downloadFileStream = File.OpenWrite(localPath);
                download.Content.CopyTo(downloadFileStream);
                
                downloadFileStream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception");
                if (e is PathTooLongException || e is DirectoryNotFoundException || e is UnauthorizedAccessException || e is ArgumentNullException | e is ObjectDisposedException | e is NotSupportedException | e is IOException)
                {
                    Console.WriteLine(e.Message);
                }
                else
                    throw;
            }
        }
    }
}
