﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Authentication;
using amelie.Model;
using System.Text;
using amelie.IdentityDocument;
using System.Linq;
using System.Collections;
using amelie.Decoding;
using Microsoft.Extensions.Configuration;

namespace amelie.DAL
{
    public class DAMongoDB
    {

        private static IMongoClient mongoClient;
        public static IMongoDatabase database { get; set; }
        public static IMongoCollection<BsonDocument> messagesCollection { get; set; }
        public static IMongoCollection<Model.IdentityDocument> idDocCollection { get; set; }
        public static IMongoCollection<ReferenceEngine> referenceEngineCollection { get; set; }

        public static IMongoCollection<BatchJob> batchJobCollection { get; set; }

        public const string INTERRUPTED = "Interrupted";
        public static void openConnection()
        {
            string connectionString = ConfigurationManager.AppSettings["connectionSNCB"];
            MongoClientSettings settings = MongoClientSettings.FromUrl(
              new MongoUrl(connectionString)
            );
            settings.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            mongoClient =  new MongoClient(settings);
            string databaseName = ConfigurationManager.AppSettings["databaseSNCB"];
            database = mongoClient.GetDatabase(databaseName);
        }

        public static void connectionLocal()
        {
            mongoClient = new MongoClient("mongodb://localhost:27017");
            string databaseName = ConfigurationManager.AppSettings["databaseSNCB"];
            database = mongoClient.GetDatabase(databaseName);
        }
        public static void openConnectionRemote()
        {
            string connectionString = ConfigurationManager.AppSettings["connectionRemote"];
            mongoClient = new MongoClient(connectionString);
            string databaseName = ConfigurationManager.AppSettings["databaseSNCB"];
            database = mongoClient.GetDatabase(databaseName);
        }

        public static void openConnectionSNCB(IConfiguration config)
        {
            string connectionString = config["connectionMongoDB"];
            mongoClient = new MongoClient(connectionString);
            string databaseName = ConfigurationManager.AppSettings["databaseSNCB"];
            database = mongoClient.GetDatabase(databaseName);
        }

        public static void initCollections()
        {
            initMessagesCollection();
            initIdentityDocumentCollection();
            initReferenEngineCollection();
            initBatchJobCollection();
        }

        private static Model.IdentityDocument createIdentityDocumentObject(string fileNameParam,
                                            string linkParam,
                                            string vehiculeT,
                                            string vehiculeN,
                                            DecodingStatusEnum status,
                                            string jruFormat,
                                            string nidEngine,
                                            DateTime filedate,
                                            DateTime uploadDate,
                                            string localpath)
        {
            Model.IdentityDocument idDoc = new Model.IdentityDocument
            {
                fileName = fileNameParam,
                link = linkParam,
                vehiculeType = vehiculeT,
                vehiculeNumber = vehiculeN,
                decodingProcess = new DecodingProcess { decodingStatus = status },
                decoderParameter = jruFormat,
                nidEngine = nidEngine,
                //dateTimeFirstMessage ="",
                //dateTimeLastMessage = "",
                fileDate = filedate,
                uploadDate = uploadDate,
                localpath = localpath
            };
            return idDoc;
        }

        public static void insertOneIdDoc(string fileNameParam,
                                            string linkParam,
                                            string vehiculeT,
                                            string vehiculeN,
                                            DecodingStatusEnum status,
                                            string jruFormat,
                                            string nidEngine,
                                            DateTime filedate,
                                            DateTime uploadDate,
                                            string localpath)
        {
            Model.IdentityDocument idDoc = createIdentityDocumentObject(fileNameParam,
                                                                          linkParam, vehiculeT, vehiculeN, status, jruFormat, nidEngine, filedate, uploadDate, localpath);
            idDocCollection.InsertOne(idDoc);
        }

        private static ReferenceEngine createReferenceEngine(string HV, 
                                                            string NID_ENGINE, 
                                                            string ENGINE_TYPE, 
                                                            string JRU_TYPE, 
                                                            DateTime valid_from, 
                                                            string decodeParam, 
                                                            string pid)
        {

            ReferenceEngine refEngine = new ReferenceEngine
            {
                HV = HV,
                NID_ENGINE = NID_ENGINE,
                ENGINE_TYPE = ENGINE_TYPE,
                JRU_TYPE = JRU_TYPE,
                decodingconfig = new DecodingConfig[] { new DecodingConfig { valid_from = valid_from, decodeparemeter = decodeParam, pid = pid } }

            };
            return refEngine;

        }

        public static void insertOneReferenceEngine(string HV,
                                                    string NID_ENGINE,
                                                    string ENGINE_TYPE,
                                                    string JRU_TYPE,
                                                    DateTime valid_from,
                                                    string decodeparemeter,
                                                    string pid)
        {
            referenceEngineCollection.InsertOne(createReferenceEngine(HV, NID_ENGINE, ENGINE_TYPE, JRU_TYPE, valid_from, decodeparemeter, pid));
        }
      
        private static bool existsCollection(string collectionName)
        {
            var filter = new BsonDocument("name", collectionName);
            var options = new ListCollectionNamesOptions { Filter = filter };
            return database.ListCollectionNames(options).Any();
        }
        public static bool existsIdentityDocumentCollection()
        {
            string idDoc = ConfigurationManager.AppSettings["idDoc"];
            return existsCollection(idDoc);
        }
        public static bool existsReferenEngineCollection()
        {
            string referenceEngine = ConfigurationManager.AppSettings["referenceEngine"];
            return existsCollection(referenceEngine);
        }
        private static void initMessagesCollection()
        { 
            string collectionMessage = ConfigurationManager.AppSettings["collectionMessage"];
            messagesCollection =  database.GetCollection<BsonDocument>(collectionMessage);
        }

        private static void initIdentityDocumentCollection()
        {
            string idDoc = ConfigurationManager.AppSettings["idDoc"];
            idDocCollection = database.GetCollection<Model.IdentityDocument>(idDoc);
        }

        private static void initReferenEngineCollection()
        {
            string referenceEngine = ConfigurationManager.AppSettings["referenceEngine"];
            referenceEngineCollection = database.GetCollection<ReferenceEngine>(referenceEngine);
        }

        private static void initBatchJobCollection()
        {
            string batch = ConfigurationManager.AppSettings["batch"];
            batchJobCollection = database.GetCollection<BatchJob>(batch);
        }

        public static Model.IdentityDocument getIdDocByFileName(string fileName)
        {
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            var res = idDocCollection.Find(filter).ToList();
            return (res.Count > 0) ? res.First() : null;
        }

        public static Model.ReferenceEngine getReferenceEngineByHV(string hv)
        {
            var filter = Builders<Model.ReferenceEngine>.Filter.Eq("HV", hv);
            var res = referenceEngineCollection.Find(filter).ToList();
            return (res.Count > 0) ? res.First() : null;
        }

        public static string getNIDEngineFromDecodedMessage(string fileName)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("FileName", fileName);
            var res = messagesCollection.Find(filter).Limit(1).ToList();
            if (res.Count == 0)
                return "";
            if (res.First().Contains("NID_ENGINE"))
                return res.First().GetValue("NID_ENGINE").ToString();
            else
                return "";
        }
        public static List<Model.IdentityDocument> getIdDocsByStatus(int status)
        {
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("decodingProcess.decodingStatus", status);
            return idDocCollection.Find(filter).ToList();
        }
        public static void updateIdDocStatusToBeStarted(string fileName)
        {

            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            var update = Builders<Model.IdentityDocument>.Update.Set("decodingProcess.decodingStatus", DecodingStatusEnum.TobeStarted);

            idDocCollection.UpdateOne(filter, update);
        }

        public static void updateIdDocStatusOnGoing (string fileName, string logLink)
        {
            /*$Y refuse the update the loglink until having more clarification */
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            var update = Builders<Model.IdentityDocument>.Update.Set("decodingProcess.decodingStatus", DecodingStatusEnum.OnGoing)
                                                                 .Set("decodingProcess.processingDate", DateTime.Now)
                                                                 .Set("loglink", logLink);
            idDocCollection.UpdateOne(filter, update);
        }

        public static void updateIdDocStatusFailed(string fileName)
        {
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            var update = Builders<Model.IdentityDocument>.Update.Set("decodingProcess.decodingStatus", DecodingStatusEnum.Failed);
            idDocCollection.UpdateOne(filter, update);
        }

        public static void updateIdDocStatusToTakenByABatch(string fileName)
        {
            /* Only set ID Doc to be taken by a Batch if its status is Forced(taken by an immediate Batch) 
             or To be started ( taken by an scheduled Batch). This avoids Id Doc to be reset to taken by a batch when a Batch being resumed */
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            filter = filter & (Builders<Model.IdentityDocument>.Filter.Eq("decodingProcess.decodingStatus", DecodingStatusEnum.Forced) 
                | Builders<Model.IdentityDocument>.Filter.Eq("decodingProcess.decodingStatus", DecodingStatusEnum.TobeStarted));
            var update = Builders<Model.IdentityDocument>.Update.Set("decodingProcess.decodingStatus", DecodingStatusEnum.TakenByARunningBatch);
            idDocCollection.UpdateOne(filter, update);
        }
        public static void updateIdDocStatusSuccessful (string fileName, string NID_ENGINE, DateTime firstMessage, DateTime lastMessage)
        {
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            var update = Builders<Model.IdentityDocument>.Update.Set("decodingProcess.decodingStatus", DecodingStatusEnum.Successful)
                                                                .Set("nidEngineJRU", ((NID_ENGINE == "")? "Not found" :  NID_ENGINE))
                                                                .Set("dateTimeFirstMessage", firstMessage)
                                                                .Set("dateTimeLastMessage", lastMessage);
            idDocCollection.UpdateOne(filter, update);
        }

        public static void updateIdDocNumbersMessages(string fileName, int numberMessage, int errorMessage)
        {
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            var update = Builders<Model.IdentityDocument>.Update.Set("decodingProcess.numberMessage", numberMessage)
                                                                .Set("decodingProcess.errorMessage", errorMessage);
            idDocCollection.UpdateOne(filter, update);
        }

        public static void updateIdDocDatesMessages(string fileName, string nid, DateTime firstMessage, DateTime lastMessagee)
        {
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            var update = Builders<Model.IdentityDocument>.Update.Set("dateTimeFirstMessage", firstMessage)
                                                                .Set("dateTimeLastMessage", lastMessagee)
                                                                .Set("nidEngineJRU", nid);
            idDocCollection.UpdateOne(filter, update);
        }

        public static void updateIdDocDecodingParam (string fileName, string decoderParam)
        {
            /*$Y refuse the update the loglink until having more clarification */
            var filter = Builders<Model.IdentityDocument>.Filter.Eq("fileName", fileName);
            var update = Builders<Model.IdentityDocument>.Update.Set("decoderParameter", decoderParam);
            idDocCollection.UpdateOne(filter, update);
        }

        public static string findDecodingParameterByDate (string vehiculeNumber, DateTime date)
        {

            var filter = new BsonDocument("HV", vehiculeNumber);
            var res =  referenceEngineCollection.Find(filter)
                                                .Project(
                                                            x => x.decodingconfig.Select(y => y)
                                                            .Where(z => (date.CompareTo(z.valid_from) > -1))
                                                            .OrderByDescending(d => d.valid_from)
                                                         )
                                                .Limit(1)
                                                .FirstOrDefault();
            /* DEBUG */
            //foreach (var v in res)
            //    Console.WriteLine("date:{0}, decode:{1}", v.valid_from, v.decodeparemeter);

            return (res != null )? res.First().decodeparemeter : "";

        }

        public class DatesMessages
        {
            public DateTime firstDate { get; set; }
            public DateTime lastDate { get; set; }
        }
        //public static DatesMessages GetFirstLastDateMessage(string fileName)
        //{
        //    DatesMessages res = new DatesMessages();
        //    var pipeline = new BsonDocument[] { 
        //        new BsonDocument { { "$match", new BsonDocument ("FileName", fileName) } },
        //        new BsonDocument { { "$group", new BsonDocument {
        //                                                            {"_id", "$FileName" },
        //                                                            {"Min", new BsonDocument { {"$min", "$Time"} } },
        //                                                            {"Max", new BsonDocument { {"$max", "$Time"} } }
        //                                                        }
        //                          } }
        //    };
        //    var r = messagesCollection.Aggregate<BsonDocument>(pipeline).First();
        //    res.firstDate = DateTime.Parse(r.GetValue("Min").ToString());
        //    res.lastDate = DateTime.Parse(r.GetValue("Max").ToString());
        //    return res;
        //}

        public static DatesMessages GetFirstLastDateMessage(string fileName)
        {
            DatesMessages res = new DatesMessages();
            var filter = new BsonDocument("FileName", fileName);
            var min = messagesCollection.Find(filter).Sort(new BsonDocument("Time", 1)).Limit(1).FirstOrDefault();

            var max = messagesCollection.Find(filter).Sort(new BsonDocument("Time", -1)).Limit(1).FirstOrDefault();


            res.firstDate = (min != null && min.Contains("Time"))? ((DateTime) min.GetValue("Time")) : DateTime.MinValue;
            res.lastDate = (max != null && max.Contains("Time"))? (DateTime)max.GetValue("Time") : DateTime.MinValue;
            return res;
        }

        public static List<Model.IdentityDocument> findIdDocByFilter(string f)
        {
            var filter = BsonDocument.Parse(f);
            return idDocCollection.Find(filter).ToList();
        }

        public static BatchJob findBatchByID (string id)
        {
            var res = batchJobCollection.Find(new BsonDocument("uid", id)).Limit(1).ToList();
            return (res.Count > 0) ? res.First() : null;
        }

        public static BatchJob findBatchByFileName(string fileName)
        {
            var filter = new BsonDocument("batchItems.fileName", fileName);
            var res = batchJobCollection.Find(filter).Sort(new BsonDocument("startDate", -1)).ToList();
            return (res.Count > 0) ? res.First() : null;
        }
        public static void refreshExistingBatchJob(BatchJob newBatchJob)
        {
            var filter = Builders<BatchJob>.Filter.Eq("uid", newBatchJob.uid);

            var update = Builders<BatchJob>.Update.Set("batchItems", newBatchJob.batchItems)
                                                  .Set("numberFilestoTreat", newBatchJob.batchItems.Count())
                                                  .Set("numberFilesinError", 0)
                                                  .Set("numberFilesTreated", 0)
                                                  .Set("startDate", DateTime.MinValue)
                                                  .Set("endDate", DateTime.MinValue)
                                                     ;
            batchJobCollection.UpdateOne(filter, update);
        }

        public static void insertBatchJob (BatchJob batchJob)
        {
            batchJobCollection.InsertOne(batchJob);
        }

        public static void setBatchToInterrupted (BatchJob batchJob)
        {
            var filter = Builders<BatchJob>.Filter.Eq("uid", batchJob.uid);

            var update = Builders<BatchJob>.Update.Set("status", INTERRUPTED)
                                                  .Set("endDate", DateTime.Now);
            batchJobCollection.UpdateOne(filter, update);
        }

        /* When a batch is stopped, its status is set to stopping and will be passed to
         intterupted when it finishes decoding its current file*/
        public static void setBatchToStop(string uid)
        {
            var filter = Builders<BatchJob>.Filter.Eq("uid", uid);

            var update = Builders<BatchJob>.Update.Set("status", "Stopping");

            batchJobCollection.UpdateOne(filter, update);
        }

        /* When an interrupted batch releases all its files. It mark this status to finish all its activities*/
        public static void setBatchToReleased(string uid)
        {
            var filter = Builders<BatchJob>.Filter.Eq("uid", uid);

            var update = Builders<BatchJob>.Update.Set("status", "Released");

            batchJobCollection.UpdateOne(filter, update);
        }

        public static void setBatchToStart(BatchJob batchJob)
        {
            var filter = Builders<BatchJob>.Filter.Eq("uid", batchJob.uid);

            var update = Builders<BatchJob>.Update.Set("status", "on Going")
                                                  .Set("startDate", DateTime.Now);
            batchJobCollection.UpdateOne(filter, update);
        }

        public static void setBatchItemToFailed(string uid, BatchItem batchItem)
        {
            string fileName = batchItem.fileName;
            var filter = new BsonDocument ("uid", uid);
            filter.Add("batchItems.fileName", fileName);

            var update = Builders<BatchJob>.Update.Set("batchItems.$.errorNumber", 1)
                                                  .Set("batchItems.$.status", "Failed")
                                                  .Inc("numberFilesinError", 1);
            batchJobCollection.UpdateOne(filter, update);
        }

        public static void setBatchItemToOnGoing(string uid, BatchItem batchItem)
        {
            /*$Y status should be an enum, the batch item position is redundant to locate the file */
            string fileName = batchItem.fileName;
            var filter = new BsonDocument("uid", uid);
            filter.Add("batchItems.fileName", fileName);

            var update = Builders<BatchJob>.Update.Set("batchItems.$.status", "on Going");
            batchJobCollection.UpdateOne(filter, update);
        }

        public static void setBatchItemToSuccessfull(string uid, BatchItem batchItem)
        {
            string fileName = batchItem.fileName;
            var filter = new BsonDocument("uid", uid);
            filter.Add("batchItems.fileName", fileName);

            var update = Builders<BatchJob>.Update.Set("batchItems.$.status", "Success");
            batchJobCollection.UpdateOne(filter, update);
        }
        public static void setBatchToDone(BatchJob batchJob)
        {
            var filter = new BsonDocument("uid", batchJob.uid);
            var update = Builders<BatchJob>.Update.Set("status", "Done")
                                                  .Set("endDate", DateTime.Now);
            batchJobCollection.UpdateOne(filter, update);
        }
        public static void incrementNumberFileTreated(string uid)
        {
            var filter = Builders<BatchJob>.Filter.Eq("uid", uid);

            var update = Builders<BatchJob>.Update.Inc("numberFilesTreated", 1);
            batchJobCollection.UpdateOne(filter, update);
        }

        public static BatchJob getLastBatchScheduled()
        {
            List<BatchJob> res = batchJobCollection.Find(_ => true).Sort(new BsonDocument("plannedStartDate", -1)).ToList();
            return (res.Count > 0) ? res.First() : null;
        }
    }
}
