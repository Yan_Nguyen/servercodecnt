$kubernetesResourceGroup="amelie" # needs to be unique to your subscription
$acrName='amelieCR' #must conform to the following pattern: '^[a-zA-Z0-9]*$
$aksClusterName='amelie-cluster'
$location = 'westeurope'
$numberOfNodes = 2 # In production, you're going to want to use at least three nodes.

az group create -l $location -n $kubernetesResourceGroup

az acr create --resource-group $kubernetesResourceGroup --name $acrName --sku Standard --location $location

$sp= az ad sp create-for-rbac --skip-assignment | ConvertFrom-Json
$appId = $sp.appId
$appPassword = $sp.password

Start-Sleep -Seconds 180

$acrID=az acr show --resource-group $kubernetesResourceGroup --name $acrName --query "id" --output tsv

az role assignment create --assignee $appId --scope $acrID --role acrpull

$version=$(az aks get-versions -l westeurope --query 'orchestrators[-1].orchestratorVersion' -o tsv)
az aks create `
   --resource-group $kubernetesResourceGroup `
   --name $aksClusterName `
   --node-count $numberOfNodes `
   --network-plugin azure
   --vnet-subnet-id <subnet-id>
   --service-principal $appId `
   --client-secret $appPassword `
   --generate-ssh-keys `
   --location $location `
   --kubernetes-versuib $version `
   --max-pods 250

az network vnet create `
    --resource-group amelie `
    --name myAKSVnet `
    --address-prefixes 192.168.0.0/16 `
    --subnet-name myAKSSubnet `
    --subnet-prefix 192.168.0.0/23 `
    --location westeurope

az ad sp create-for-rbac --skip-assignment

$VNET_ID=$(az network vnet show --resource-group amelie --name myAKSVnet --query id -o tsv)
$SUBNET_ID=$(az network vnet subnet show --resource-group amelie --vnet-name myAKSVnet --name myAKSSubnet --query id -o tsv)

az role assignment create --assignee 59219eee-c3be-48bd-bc27-e9e83936e795 --scope $VNET_ID --role Contributor

az aks create `
    --resource-group amelie `
    --node-count 2 `
    --kubernetes-version 1.15.7 `
    --location westeurope `
    --enable-addons monitoring `
    --name amelie-cluster `
    --vnet-subnet-id $SUBNET_ID `
    --docker-bridge-address 172.17.0.1/16 `
    --dns-service-ip 10.0.0.10 `
    --service-cidr 10.0.0.0/16 `
    --generate-ssh-keys `
    --max-pods 250 `
    --windows-admin-password P@ssw0rd1234 `
    --windows-admin-username amelie `
    --vm-set-type VirtualMachineScaleSets `
    --network-plugin azure `
    --load-balancer-sku standard `
    --service-principal 90bd8b2f-680d-499f-b47a-5ffd4c62c835 `
    --client-secret fa19330d-998a-4ee6-a899-742c7a10dc71

az aks nodepool add `
    --resource-group amelie `
    --cluster-name amelie-cluster `
    --os-type Windows `
    --name npwin `
    --node-count 2 `
    --kubernetes-version 1.15.7

az aks get-credentials --resource-group amelie --name amelie-cluster


helm install stable/nginx-ingress --namespace kube-system --set controller.replicaCount=2 --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux  --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux

helm install nginx-ingress stable/nginx-ingress `
    --namespace kube-system `
    --set controller.replicaCount=2 `
    --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux `
    --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux