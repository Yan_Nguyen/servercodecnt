FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app
COPY ["ServerCodecNT.csproj", ""]
RUN dotnet restore "./ServerCodecNT.csproj"
COPY . /app
WORKDIR "/app"
RUN dotnet build "ServerCodecNT.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ServerCodecNT.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ADD decoderLibs /app/decoderLibs
ENTRYPOINT ["dotnet", "ServerCodecNT.dll"]